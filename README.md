# Overview

This is a simple API-key based e-mail form handler. It'll look up the
destination address and the redirect page by the API key so that the
destination e-mail or redirect page will never be selected by
malicious input.

All e-mails sent are sent as wrapped UTF-8 text/plain. The subject
lines will be prefixed with "[Contact Request]".

# API

## Request Format

POST form data to:

/api/v1.0/formail

Will return a 303 Redirect to the per-application redirect landing
page.

## Required Fields

* apikey: the API key associated with the contactee.
* name: the name of the contactor
* phone: the phone number of the contactor
* subject: the subject line of the e-mail
* body: the additional information from the contactor
