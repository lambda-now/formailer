-- Application key schema

create or replace function migrations.apply_00003_appkey_schema()
returns bigint
as $$

begin

        create table application.appkey (
               key_ripemd160 varchar(40) not null,
               client_name varchar(200) not null,
               application_name varchar(200) not null,

               created_at timestamp with time zone not null default current_timestamp,
               expires_at timestamp with time zone not null,

               primary key (key_ripemd160, client_name, application_name),
               foreign key (client_name, application_name) references application.application (client_name, application_name)
               );

        create unique index appkey_key_ripemd160_idx on application.appkey (key_ripemd160);
        create index appkey_client_name_idx on application.appkey (client_name);
        create index appkey_application_name_idx on application.appkey (application_name);
        create index appkey_created_at_idx on application.appkey (created_at);
        create index appkey_expires_at_idx on application.appkey (expires_at);

return 0;
end;
$$ language plpgsql;

select migrations.add_migration(3, 'appkey_schema', 2, 'app_schema');
