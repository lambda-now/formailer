-- Client applications

create or replace function migrations.apply_00002_app_schema()
returns bigint
as $$

begin

        create schema application;

        create table application.application (
               client_name varchar(200) not null references client.client (client_name),
               application_name varchar(200) not null,

               sender_email varchar(200) not null,
               redirect_to_url varchar(512) not null,
               created_at timestamp with time zone not null default current_timestamp,
               primary key (client_name, application_name)
               );

        create index application_client_name_idx on application.application (client_name);
        create index application_application_name_idx on application.application (application_name);

        create index application_created_at_idx on application.application (created_at);

return 0;
end;
$$ language plpgsql;

select migrations.add_migration(2, 'app_schema', 1, 'client_schema');
