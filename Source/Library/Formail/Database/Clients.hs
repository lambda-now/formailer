{-# Language OverloadedStrings, QuasiQuotes #-}

module Formail.Database.Clients (insertClient, getClient, updateClient) where

import Formail.Types.Client
import Formail.Database.Queries
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.SqlQQ
import Data.Text (Text)

import Formail.Types

insertClientQuery :: Query
insertClientQuery = "INSERT INTO client.client (client_name, client_email, client_contact_name) VALUES (?, ?, ?) RETURNING *"

insertClient :: Connection -> Client -> IO ((Saved Client), Bool)
insertClient conn cli = do
  c_ <- getClient conn $ clientName cli

  case c_ of
    Nothing -> do
      c <- one conn insertClientQuery (clientName cli, clientEmail cli, clientContact cli)
      return $ (c, True)
    (Just c) -> return (c, False)

getClientQuery :: Query
getClientQuery = "SELECT * FROM client.client WHERE client_name = ?"

getClient :: Connection -> Text -> IO (Maybe (Saved Client))
getClient conn name = maybeOne conn getClientQuery [name]

updateClientQuery :: Query
updateClientQuery = [sql|
                        UPDATE client.client set client_email=?, client_contact_name=? where client_name=?
                        RETURNING *
                        |]

updateClient ::
                      Connection -> Client -> IO (Maybe (Saved Client))
updateClient conn (Client cname cemail ccontact) =
  maybeOne conn updateClientQuery (cname, cemail, ccontact)
