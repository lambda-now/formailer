-- Options for Keys commands.

module Formail.Options.Keys where

import Options.Applicative
import Formail.Types
import Formail.Options.Common
import Formail.Options.Clients (clientNameOption)
import Formail.Options.Applications (applicationNameOption)
import Data.Text (Text)

data KeyCommand =
  NewKeyCommand NewKey |
  LookupAppCommand Text
  deriving (Show, Eq)

data NewKey = NewKey {
  newKeyClientName :: Text,
  newKeyApplicationName :: Text
  } deriving (Show, Eq)

instance HasClientName NewKey where
  clientName = newKeyClientName

instance HasApplicationName NewKey where
  applicationName = newKeyApplicationName

keyCommand :: Parser KeyCommand
keyCommand = subparser (
  command "new" (NewKeyCommand <$> newKeyOptions `withInfo` "Issue a new application key.") <>
  command "lookup" (LookupAppCommand <$> keyOption `withInfo` "Look up the app for a key.")
  )

newKeyOptions :: Parser NewKey
newKeyOptions = NewKey <$>
  clientNameOption <*>
  applicationNameOption

keyOption :: Parser Text
keyOption =
  textOption (long "key" <>
              metavar "KEY" <>
              help "Application key to look up.")
