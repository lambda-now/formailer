-- Helper functions for running queries.

{-# Language FlexibleContexts #-}

module Formail.Database.Queries (one, maybeOne, Saved(..), IntegrityError(..), callFunc, callFunc', execFunc, execFunc') where

import Data.Time.Clock (UTCTime)
import Data.Time.Format (formatTime, defaultTimeLocale, iso8601DateFormat)
import Control.Exception (Exception, throw)
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow

data IntegrityError = NoRowsReturned | MultipleRowsReturned
  deriving (Show, Eq)
instance Exception IntegrityError

data CallError = NoReturnValue | MultipleValuesReturned | CallFailed Int
  deriving (Show, Eq)
instance Exception CallError

one :: (FromRow a, ToRow q) => Connection -> Query -> q -> IO a
one conn q p = justOne <$> maybeOne conn q p
  where
    justOne Nothing = throw NoRowsReturned
    justOne (Just r) = r

maybeOne :: (FromRow a, ToRow q) => Connection -> Query -> q -> IO (Maybe a)
maybeOne conn q p = mone <$> query conn q p
  where
    mone [] = Nothing
    mone (r:[]) = Just r
    mone (_:_) = throw MultipleRowsReturned

-- | Calls a function that should return 0 on success.
callFunc :: (ToRow a) => Connection -> Query -> a ->  IO (Either Int ())
callFunc conn q p = query conn q p >>= res
  where
    res [] = throw NoReturnValue
    res (Only i:[]) = if (i == 0) then return (Right ()) else return (Left i)
    res (_:_) = throw MultipleValuesReturned

-- | Calls a function with no parameters that should return 0 on success.
callFunc' :: Connection -> Query -> IO (Either Int ())
callFunc' conn q = query_ conn q >>= res
  where
    res [] = throw NoReturnValue
    res (Only i:[]) = if (i == 0) then return (Right ()) else return (Left i)
    res (_:_) = throw MultipleValuesReturned

-- | Calls a function and throws an error if it returns !0
execFunc :: (ToRow a) => Connection -> Query -> a ->  IO ()
execFunc conn q p = callFunc conn q p >>= res
  where
    res (Left e) = throw $ CallFailed e
    res (Right ()) = return ()

-- | Calls a function with no parameters and throws an error if it returns !0
execFunc' :: Connection -> Query ->  IO ()
execFunc' conn q = callFunc' conn q >>= res
  where
    res (Left e) = throw $ CallFailed e
    res (Right ()) = return ()

data Saved a = Saved a UTCTime

instance (FromRow a) => FromRow (Saved a) where
  fromRow = Saved <$> fromRow <*> field

instance (Show a) => Show (Saved a) where
  show (Saved a t) = "Saved@" ++ (formatTime defaultTimeLocale (iso8601DateFormat Nothing) t) ++ " " ++ show a
