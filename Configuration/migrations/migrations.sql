create schema migrations;

create table migrations.migrations (
       migration_nbr bigint not null check (migration_nbr >= 0),
       migration_name varchar not null,

       depends_on_nbr bigint,
       depends_on_name varchar,

       applied_at TIMESTAMP WITH TIME ZONE,

       created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,

       primary key (migration_nbr, migration_name),
       foreign key (depends_on_nbr, depends_on_name) references migrations.migrations (migration_nbr, migration_name)
 );

create index migrations_migration_nbr_idx on migrations.migrations (migration_nbr);
create index migrations_migration_name_idx on migrations.migrations (migration_name);

create index migrations_depends_on_nbr_idx on migrations.migrations (depends_on_nbr);
create index migrations_depends_on_name_idx on migrations.migrations (depends_on_name);

insert into migrations.migrations
(migration_nbr, migration_name, depends_on_nbr, depends_on_name, applied_at)
values
(0, 'root', NULL, NULL, now());

create view migrations.migrations_graph as
with recursive migration_graph(migration_nbr, migration_name, depends_on_nbr, depends_on_name, applied_at) as (
select migration_nbr, migration_name, depends_on_nbr, depends_on_name, applied_at from migrations.migrations
where depends_on_nbr is NULL and depends_on_name is NULL
union all
select q.migration_nbr, q.migration_name, q.depends_on_nbr, q.depends_on_name, q.applied_at
from migrations.migrations p, migrations.migrations q
where p.migration_nbr = q.depends_on_nbr and p.migration_name = q.depends_on_name)
select * from migration_graph;

CREATE OR REPLACE FUNCTION migrations.run_migrations_to(target_nbr bigint, target_name character varying)
 RETURNS bigint
 LANGUAGE plpgsql CALLED ON NULL INPUT
AS $function$

declare mig record;

begin

   for mig in select migration_nbr, migration_name from migrations.migrations_graph where applied_at is null loop
       raise notice 'Applying migration %:%', mig.migration_nbr, mig.migration_name;
       execute format('select  migrations.apply_%s_%s();', trim(from to_char(mig.migration_nbr, 'FM00000')), mig.migration_name);
       update migrations.migrations set applied_at = now() where migration_nbr = mig.migration_nbr and migration_name = mig.migration_name;
       if target_nbr is not null and target_name is not null and mig.migration_nbr=target_nbr and mig.migration_name = target_name then
          return 0;
       end if;
       end loop;
   return 0;
end;
$function$;

CREATE OR REPLACE FUNCTION migrations.run_all_migrations()
returns bigint
language plpgsql
as $function$
begin

return sum(migrations.run_migrations_to(null, null));

end;
$function$;

create or replace function migrations.add_migration(migration_nbr bigint, migration_name varchar,
       depends_on_nbr bigint, depends_on_name varchar)
returns bigint
as $function$
declare mig record;
declare nbr bigint;
declare name varchar;
begin
        nbr := migration_nbr;
        name := migration_name;
        select * into mig from migrations.migrations as m where m.migration_nbr = nbr and m.migration_name = name;
        if not found then
               insert into migrations.migrations (migration_nbr, migration_name, depends_on_nbr, depends_on_name) values
                      (migration_nbr, migration_name, depends_on_nbr, depends_on_name);
        end if;
        return 0;
end;
$function$ language plpgsql;

select 0;
