-- Options for client commands.

module Formail.Options.Clients
       (ClientCommand(..), EditClient(..), clientCommand, clientNameOption, editedClient)
       where

import Options.Applicative
import Formail.Options.Common
import Formail.Types
import Data.Text (Text)

data ClientCommand =
  NewClientCommand Client |
  EditClientCommand EditClient
  deriving (Show, Eq)

data EditClient = EditClient {
  editClientName :: Text,
  editClientEmail :: Maybe Text,
  editClientContactName :: Maybe Text
  }
  deriving (Show, Eq)

editedClient :: EditClient -> Client -> Client
editedClient (EditClient _ nemail ncname) (Client oname oemail ocname) =
  Client oname (maybe oemail id nemail) (maybe ocname id ncname)

clientCommand :: Parser ClientCommand
clientCommand = subparser (
  command "new" (NewClientCommand <$> newClientOptions `withInfo` "Create a new client.") <>
  command "edit" (EditClientCommand <$> editClientOptions `withInfo` "Edit an existing client.")
  )

clientNameOption :: Parser Text
clientNameOption = textOption (long "client-name" <>
             metavar "NAME" <>
             help "Unique name of client.")

clientEmailOption :: Parser Text
clientEmailOption = textOption (long "client-email" <>
                        metavar "EMAIL" <>
                        help "E-mail contact address for client.")

clientContactOption :: Parser Text
clientContactOption = textOption (long "client-contact" <>
                        metavar "CONTACT" <>
                        help "Name of contact at client.")

newClientOptions :: Parser Client
newClientOptions = Client <$>
  clientNameOption <*> clientEmailOption <*> clientContactOption

editClientOptions :: Parser EditClient
editClientOptions = EditClient <$>
  clientNameOption <*> optional clientEmailOption <*> optional clientContactOption
