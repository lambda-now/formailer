-- Messages schema

create or replace function migrations.apply_00004_messages()
returns bigint
as $$
begin
        create schema messages;

        create table messages.message (
               key_ripemd160 varchar(40) not null references application.appkey (key_ripemd160),
               message_id varchar(200) not null,

               message_date timestamp with time zone not null,
               contact_name varchar(200) not null,
               contact_phone varchar(50) not null,
               contact_email varchar(200) not null,
               subject varchar(500) not null,
               body text not null,

               created_at timestamp with time zone not null default current_timestamp,

               primary key (key_ripemd160, message_id)
               );

        create index message_application_fk_idx on messages.message (key_ripemd160);

        create unique index message_message_id_idx on messages.message (message_id);
        create index message_key_ripemd160_idx on messages.message (key_ripemd160);
        create index message_message_date_idx on messages.message (message_date);
        create index message_contact_name_idx on messages.message (contact_name);
        create index message_contact_phone_idx on messages.message (contact_phone);
        create index message_contact_email_idx on messages.message (contact_email);
        create index message_subject_idx on messages.message (subject);
        create index message_created_at_idx on messages.message (created_at);

return 0;
end;
$$ language plpgsql;

select migrations.add_migration(4, 'messages', 3, 'appkey_schema');
