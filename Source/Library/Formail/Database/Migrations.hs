-- Helper functions for dealing with database migrations

{-# Language OverloadedStrings #-}

module Formail.Database.Migrations where

import Formail.Configuration

import Formail.Database.Queries
import System.Directory
import System.FilePath
import Data.Char
import Data.List
import Data.String (fromString)
import Database.PostgreSQL.Simple

migrations :: DataFilesConfig -> IO [String]
migrations config = do
  sort <$> filter isMigration <$> getDirectoryContents (migrationsRoot config)

  where
      isMigration fp =
        let
          isSql = (snd . splitExtension $ fp) == ".sql"
          fd (x:_)= isDigit x
          fd [] = False
        in
          isSql && (fd fp)

migrationFiles :: DataFilesConfig -> IO [FilePath]
migrationFiles config =
  map ((</>) (migrationsRoot config)) <$> migrations config

initMigrationFile :: DataFilesConfig -> IO FilePath
initMigrationFile config = do
  return $ migrationsRoot config </> "migrations.sql"

runSqlFile :: Connection -> FilePath -> IO ()
runSqlFile conn path = fromString <$> readFile path >>= execFunc' conn

initMigrations :: DataFilesConfig -> Connection -> IO ()
initMigrations config conn =withTransaction conn (initMigrationFile config >>= runSqlFile conn)

loadMigrations :: DataFilesConfig -> Connection -> IO ()
loadMigrations config conn =
  withTransaction conn (migrationFiles config >>= mapM_ (runSqlFile conn))

applyMigrations :: Connection -> IO ()
applyMigrations conn =
  withTransaction conn (execFunc' conn "select migrations.run_all_migrations()")
