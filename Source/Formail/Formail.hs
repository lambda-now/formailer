{-# Language OverloadedStrings, FlexibleContexts #-}

module Main where

import Control.Monad (msum)
import Happstack.Server (Conf(..), nullConf, simpleHTTP, dirs, ServerPartT)
import Formail.Configuration
import Formail.Options
import APIv1
import Data.Text (Text)
import Formail.Logging

main :: IO ()
main =
  execParser (serverOptions `withInfo` "Run the formail server.") >>= run

run :: ServerOptions -> IO ()
run (ServerOptions configOpt) = do
  (conf, _) <- loadConfig configOpt

  let sc = nullConf {port=httpPort $ server conf}

  runWithLogging  (logging conf) $ simpleHTTP sc $ routes conf

routes ::
  FormailConfiguration -> ServerPartT IO Text
routes config = msum [
  dirs "api/v1.0" $ apiV1Handler config
  ]
